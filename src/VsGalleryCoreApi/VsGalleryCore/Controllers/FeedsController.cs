using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VsGalleryCore.Services;

namespace VsGalleryCore.Controllers
{
    [Route("api/feeds")]
    public class FeedsController : Controller
    {
        private readonly IFeedService _feedService;

        public FeedsController(IFeedService feedService)
        {
            _feedService = feedService;
        }

        [HttpGet]
        [Route("")]
        [Route("atom")]
        [Produces("application/atom+xml")]
        public async Task<ActionResult> GetFeed()
        {
            string baseUrl = $"{Request.Scheme}://{Request.Host}";

            return Content(await _feedService.GenerateFeed(baseUrl), "application/atom+xml");
        }
    }
}