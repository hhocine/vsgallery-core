using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using VsGalleryCore.Attributes;
using VsGalleryCore.Services;
using VsGalleryCore.Dtos;

namespace VsGalleryCore.Controllers
{
    [Route("api/packages")]
    public class PackagesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPackageService _packageService;

        public PackagesController(IMapper mapper, IPackageService packageService)
        {
            _mapper = mapper;
            _packageService = packageService;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Package>> GetPackages()
        {
            return _mapper.Map<IEnumerable<Package>>(await _packageService.GetPackages());
        }
        
        [HttpPost]
        [ApiKeyAuthenticationFilter]
        [DisableRequestSizeLimit]
        [Route("upload")]
        public async Task<Package> Upload(IFormFile file)
        {
            return _mapper.Map<Package>(await _packageService.StorePackage(file));
        }

        [HttpGet]
        [Route("download/{blobId}")]
        public async Task<FileStreamResult> Download(string blobId)
        {
            var file = await _packageService.Download(blobId);

            return new FileStreamResult(file.Stream, new MediaTypeHeaderValue(file.ContentType))
            {
                FileDownloadName = file.FileName
            };
        }

        [HttpDelete]
        [ApiKeyAuthenticationFilter]
        [Route("delete/{packageId}")]
        public async Task<IActionResult> Delete(string packageId)
        {
            await _packageService.DeletePackage(packageId);

            return NoContent();
        }
    }
}