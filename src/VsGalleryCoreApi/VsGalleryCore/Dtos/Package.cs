﻿using System;
using System.Collections.Generic;

namespace VsGalleryCore.Dtos
{
    public class Package
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Version { get; set; }
        public string PackageId { get; set; }
        public string IconId { get; set; }
        public string PreviewId { get; set; }
        public string Tags { get; set; }
        public DateTime DatePublished { get; set; }
        public IEnumerable<string> SupportedVersions { get; set; }
        public string License { get; set; }
        public string GettingStartedUrl { get; set; }
        public string ReleaseNotesUrl { get; set; }
        public string MoreInfoUrl { get; set; }

        public int DownloadCount { get; set; }
    }
}
