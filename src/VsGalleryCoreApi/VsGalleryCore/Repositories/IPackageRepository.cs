﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VsGalleryCore.Models;

namespace VsGalleryCore.Repositories
{
    public interface IPackageRepository
    {
        Task<IEnumerable<VsixPackage>> GetPackages();

        Task<VsixPackage> UploadPackage(VsixPackage package, string workingDirectory, string packagePath);

        Task<FileInformation> Download(string blobId);

        Task Delete(string packageId);
    }
}
