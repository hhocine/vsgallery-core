﻿using Microsoft.AspNetCore.StaticFiles;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VsGalleryCore.Models;
using VsGalleryCore.Core;

namespace VsGalleryCore.Repositories
{
    public class PackageRepository : IPackageRepository
    {
        private readonly IMongoCollection<VsixPackage> _packages;
        private readonly GridFSBucket _bucket;

        public PackageRepository()
        {
            var client = new MongoClient(VsGalleryConfigurationLoader.Configuration.MongoConnection);
            var database = client.GetDatabase(VsGalleryConfigurationLoader.Configuration.MongoDatabaseName);

            _bucket = new GridFSBucket(database);

            _packages = database.GetCollection<VsixPackage>("packages");
        }

        public async Task<IEnumerable<VsixPackage>> GetPackages()
        {
            var result = await _packages.FindAsync(_ => true);
            return await result.ToListAsync();
        }

        public async Task<VsixPackage> UploadPackage(VsixPackage package, string workingDirectory, string packagePath)
        {
            var builder = Builders<VsixPackage>.Filter;
            var filter = builder.Eq(x => x.ID, package.ID) & builder.Eq(x => x.Version, package.Version);

            var packages = await _packages.FindAsync(filter);

            var result = await packages.FirstOrDefaultAsync();

            if (result != null)
            {
                return result;
            }

            using (var stream = File.OpenRead(packagePath))
            {
                package.PackageBlobId = await _bucket.UploadFromStreamAsync(Path.GetFileName(packagePath), stream, GetGridFSUploadOptions(Path.GetFileName(packagePath)));
            }
            
            
            if(package.Icon != null)
            {
                var icon = Path.Combine(workingDirectory, package.Icon).Replace('\\', Path.DirectorySeparatorChar);
                using (var stream = File.OpenRead(icon))
                {
                    package.IconBlobId = await _bucket.UploadFromStreamAsync(Path.GetFileName(icon), stream, GetGridFSUploadOptions(Path.GetFileName(icon)));
                }
            }

            if(package.Preview != null)
            {
                var preview = Path.Combine(workingDirectory, package.Preview).Replace('\\', Path.DirectorySeparatorChar);
                using (var stream = File.OpenRead(preview))
                {
                    package.PreviewBlobId = await _bucket.UploadFromStreamAsync(Path.GetFileName(preview), stream, GetGridFSUploadOptions(Path.GetFileName(preview)));
                }
            }

            await _packages.InsertOneAsync(package);

            return package;
        }

        public async Task<FileInformation> Download(string blobId)
        {
            var filter = Builders<GridFSFileInfo<ObjectId>>
                                   .Filter.Eq(x => x.Id, new ObjectId(blobId));

            var searchResult = await _bucket.FindAsync(filter);

            var fileEntry = searchResult.FirstOrDefault();
            var stream = new MemoryStream();
            await _bucket.DownloadToStreamAsync(fileEntry.Id, stream);

            stream.Position = 0;

            await UpdatePackageDownloadCounter(blobId);

            return new FileInformation
            {
                FileName = fileEntry.Filename,
                ContentType = GetContentType(fileEntry.Filename),
                Stream = stream
            };
        }

        public async Task Delete(string packageId)
        {
            var builder = Builders<VsixPackage>.Filter;
            var filter = builder.Eq(x => x.ID, packageId);

            var packages = await _packages.FindAsync(filter);

            var package = await packages.FirstOrDefaultAsync();

            if (package == null) return;

            await _bucket.DeleteAsync(package.PackageBlobId);
            await _bucket.DeleteAsync(package.IconBlobId);
            await _bucket.DeleteAsync(package.PreviewBlobId);

            await _packages.DeleteOneAsync(filter);
        }

        private GridFSUploadOptions GetGridFSUploadOptions(string fileName)
        {
            return new GridFSUploadOptions
            {
                Metadata = new BsonDocument {
                    { "filename", fileName },
                    { "contentType", GetContentType(fileName) }
                }
            };
        }

        private string GetContentType(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(fileName, out string contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        private async Task UpdatePackageDownloadCounter(string blobId)
        {
            var searchBuilder = Builders<VsixPackage>.Filter;
            var updateBuilder = Builders<VsixPackage>.Update;

            var searchFilter = searchBuilder.Eq(x => x.PackageBlobId, new ObjectId(blobId));
            var updateFilter = updateBuilder.Inc(x => x.DownloadCount, 1);

            var packages = await _packages.FindAsync(searchFilter);

            await _packages.FindOneAndUpdateAsync(searchFilter, updateFilter);
        }
    }
}
