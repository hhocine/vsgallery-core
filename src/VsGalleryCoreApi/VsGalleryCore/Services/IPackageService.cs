﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using VsGalleryCore.Models;

namespace VsGalleryCore.Services
{
    public interface IPackageService
    {
        Task<VsixPackage> StorePackage(IFormFile file);

        Task<FileInformation> Download(string blobId);

        Task<IEnumerable<VsixPackage>> GetPackages();

        Task DeletePackage(string packageId);
    }
}
