﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using VsGalleryCore.Models;
using VsGalleryCore.Repositories;
using VsGalleryCore.Core;
using Flurl;

namespace VsGalleryCore.Services
{
    public class FeedService : IFeedService
    {
        private readonly IPackageRepository _packageRepository;

        public FeedService(IPackageRepository packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public async Task<string> GenerateFeed(string baseUrl)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true
            };

            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("feed", "http://www.w3.org/2005/Atom");

                writer.WriteElementString("title", VsGalleryConfigurationLoader.Configuration.Title);
                writer.WriteElementString("id", VsGalleryConfigurationLoader.Configuration.Guid);
                writer.WriteElementString("updated", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                writer.WriteElementString("subtitle", VsGalleryConfigurationLoader.Configuration.Description);

                writer.WriteStartElement("link");
                writer.WriteAttributeString("rel", "alternate");
                writer.WriteAttributeString("href", baseUrl);
                writer.WriteEndElement(); // link

                var packages = await _packageRepository.GetPackages();

                foreach (VsixPackage package in packages)
                {
                    AddEntry(writer, package, baseUrl);
                }

                writer.WriteEndElement(); // feed
            }

            return sb.ToString().Replace("utf-16", "utf-8");
        }

        private void AddEntry(XmlWriter writer, VsixPackage package, string baseUrl)
        {
            var downloadUrl = baseUrl.AppendPathSegments("api", "packages", "download");

            writer.WriteStartElement("entry");

            writer.WriteElementString("id", package.ID);

            writer.WriteStartElement("title");
            writer.WriteAttributeString("type", "text");
            writer.WriteValue(package.Name);
            writer.WriteEndElement(); // title

            writer.WriteStartElement("link");
            writer.WriteAttributeString("rel", "alternate");

            var packageDownloadUrl = new Url(downloadUrl).AppendPathSegment(package.PackageBlobId).ToString();

            writer.WriteAttributeString("href", packageDownloadUrl);
            writer.WriteEndElement(); // link

            writer.WriteStartElement("summary");
            writer.WriteAttributeString("type", "text");
            writer.WriteValue(package.Description);
            writer.WriteEndElement(); // summary

            writer.WriteElementString("published", package.DatePublished.ToString("yyyy-MM-ddTHH:mm:ssZ"));
            writer.WriteElementString("updated", package.DatePublished.ToString("yyyy-MM-ddTHH:mm:ssZ"));

            writer.WriteStartElement("author");
            writer.WriteElementString("name", package.Author);
            writer.WriteEndElement(); // author

            writer.WriteStartElement("content");
            writer.WriteAttributeString("type", "application/octet-stream");
            writer.WriteAttributeString("src", packageDownloadUrl);
            writer.WriteEndElement(); // content

            writer.WriteStartElement("link");
            writer.WriteAttributeString("rel", "icon");

            var iconDownloadUrl = new Url(downloadUrl).AppendPathSegment(package.IconBlobId).ToString();

            writer.WriteAttributeString("href", iconDownloadUrl);
            writer.WriteEndElement(); // icon

            baseUrl.AppendPathSegment("api")
                .AppendPathSegment("packages")
                .AppendPathSegment("download").ToString();

            writer.WriteStartElement("link");
            writer.WriteAttributeString("rel", "previewimage");

            var previewDownloadUrl = new Url(downloadUrl).AppendPathSegment(package.PreviewBlobId).ToString();

            writer.WriteAttributeString("href", previewDownloadUrl);
            writer.WriteEndElement(); // preview

            writer.WriteStartElement("Vsix", "http://schemas.microsoft.com/developer/vsx-syndication-schema/2010");

            writer.WriteElementString("Id", package.ID);
            writer.WriteElementString("Version", package.Version);

            writer.WriteStartElement("DownloadCount");
            writer.WriteString(package.DownloadCount.ToString());
            writer.WriteEndElement();

            writer.WriteEndElement();// Vsix
            writer.WriteEndElement(); // entry
        }
    }
}
