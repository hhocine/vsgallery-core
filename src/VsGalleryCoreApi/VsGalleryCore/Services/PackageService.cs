﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using VsGalleryCore.Core;
using VsGalleryCore.Models;
using VsGalleryCore.Repositories;

namespace VsGalleryCore.Services
{
    public class PackageService : IPackageService
    {
        private readonly IPackageRepository _packageRepository;

        public PackageService(IPackageRepository packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public async Task<VsixPackage> StorePackage(IFormFile file)
        {
            var workingDirectory = Path.Combine(Path.GetTempPath(), "vsgallerycore", Guid.NewGuid().ToString());

            if (!Directory.Exists(workingDirectory))
                Directory.CreateDirectory(workingDirectory);

            try
            {
                var packagePath = Path.Combine(workingDirectory, file.FileName);

                using (var stream = new FileStream(packagePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                ZipFile.ExtractToDirectory(packagePath, workingDirectory);

                var parser = new VsixManifestParser();
                var package = parser.CreateFromManifest(workingDirectory);


                return await _packageRepository.UploadPackage(package, workingDirectory, packagePath);
            }
            finally
            {
                Directory.Delete(workingDirectory, true);
            }
        }

        public async Task<FileInformation> Download(string blobId)
        {
            return await _packageRepository.Download(blobId);
        }

        public async Task<IEnumerable<VsixPackage>> GetPackages()
        {
            return await _packageRepository.GetPackages();
        }

        public async Task DeletePackage(string packageId)
        {
            await _packageRepository.Delete(packageId);
        }
    }
}
