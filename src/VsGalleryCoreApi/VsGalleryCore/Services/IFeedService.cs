﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VsGalleryCore.Services
{
    public interface IFeedService
    {
        Task<string> GenerateFeed(string baseUrl);
    }
}
