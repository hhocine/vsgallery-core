﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VsGalleryCore.Models
{
    public class FileInformation
    {
        public string FileName { get; set; }

        public string ContentType { get; set; }

        public Stream Stream { get; set; }
    }
}
