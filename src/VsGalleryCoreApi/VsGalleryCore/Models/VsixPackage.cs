using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace VsGalleryCore.Models
{
    public class VsixPackage
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ArtifactId { get; set; }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Version { get; set; }

        [BsonIgnore]
        public string Icon { get; set; }

        [BsonIgnore]
        public string Preview { get; set; }
        public string Tags { get; set; }
        public DateTime DatePublished { get; set; }
        public IEnumerable<string> SupportedVersions { get; set; }
        public string License { get; set; }
        public string GettingStartedUrl { get; set; }
        public string ReleaseNotesUrl { get; set; }
        public string MoreInfoUrl { get; set; }

        public ObjectId IconBlobId { get; set; }
        public ObjectId PreviewBlobId { get; set; }
        public ObjectId PackageBlobId { get; set; }

        public int DownloadCount { get; set; }
    }
}