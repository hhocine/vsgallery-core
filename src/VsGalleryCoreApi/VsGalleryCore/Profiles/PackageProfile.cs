using AutoMapper;
using VsGalleryCore.Models;
using VsGalleryCore.Dtos;

namespace VsGalleryCore.Profiles
{
    public class PackageProfile : Profile
    {
        public PackageProfile()
        {
            CreateMap<VsixPackage, Package>()
                .ForMember(dto => dto.PackageId, conf => conf.MapFrom(model => model.PackageBlobId))
                .ForMember(dto => dto.IconId, conf => conf.MapFrom(model => model.IconBlobId))
                .ForMember(dto => dto.PreviewId, conf => conf.MapFrom(model => model.PreviewBlobId));
        }
    }
}