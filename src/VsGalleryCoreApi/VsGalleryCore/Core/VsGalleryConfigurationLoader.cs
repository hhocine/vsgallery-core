using System;

namespace VsGalleryCore.Core
{
	public static class VsGalleryConfigurationLoader
	{
        public static VsGalleryConfiguration Configuration => new VsGalleryConfiguration
		{
			Title = GetStringEnvironmentVariableOrDefault("VSGALLERY_TITLE", "VsGallery Core"),
			Description = GetStringEnvironmentVariableOrDefault("VSGALLERY_DESCRIPTION", "VsGallery Core"),
			Guid = GetStringEnvironmentVariableOrDefault("VSGALLERY_GUID", "7c461924-5308-480d-b048-24446c9c96e4"),
			ApiKey = GetStringEnvironmentVariableOrDefault("VSGALLERY_API_KEY",
				"h09j56z48LPSSEcNt72XaBMwaeviQ3"),
            MongoConnection = GetStringEnvironmentVariableOrDefault("VSGALLERY_MONGO_CONNECTION",
                "mongodb://root:password@localhost:27017"),
            MongoDatabaseName = GetStringEnvironmentVariableOrDefault("VSGALLERY_MONGO_DATABASE_NAME",
                "vsgallerycore")
        };

		private static string GetStringEnvironmentVariableOrDefault(string environmentVariableName, string defaultValue)
		{
			try
			{
				var value = Environment.GetEnvironmentVariable(environmentVariableName);
				return string.IsNullOrWhiteSpace(value) ? defaultValue : value;
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}
	}

    public class VsGalleryConfiguration
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Guid { get; set; }

        public string ApiKey { get; set; }

        public string MongoConnection { get; set; }

        public string MongoDatabaseName { get; set; }
    }
}