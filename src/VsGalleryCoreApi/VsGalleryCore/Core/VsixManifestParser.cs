﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using VsGalleryCore.Models;

namespace VsGalleryCore.Core
{
    public class VsixManifestParser
    {
        public VsixPackage CreateFromManifest(string workingDirectory)
        {
            string xml = File.ReadAllText(Path.Combine(workingDirectory, "extension.vsixmanifest"));
            xml = Regex.Replace(xml, "( xmlns(:\\w+)?)=\"([^\"]+)\"", string.Empty);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            VsixPackage package = new VsixPackage
            {
                ID = ParseNode(doc, "Identity", true, "Id"),
                Name = ParseNode(doc, "DisplayName", true),
                Description = ParseNode(doc, "Description", true),
                Version = new Version(ParseNode(doc, "Identity", true, "Version")).ToString(),
                Author = ParseNode(doc, "Identity", true, "Publisher"),
                Icon = ParseNode(doc, "Icon", false),
                Preview = ParseNode(doc, "PreviewImage", false),
                Tags = ParseNode(doc, "Tags", false),
                DatePublished = DateTime.UtcNow,
                SupportedVersions = GetSupportedVersions(doc),
                ReleaseNotesUrl = ParseNode(doc, "ReleaseNotes", false),
                GettingStartedUrl = ParseNode(doc, "GettingStartedGuide", false),
                MoreInfoUrl = ParseNode(doc, "MoreInfo", false)
            };

            string license = ParseNode(doc, "License", false);
            if (!string.IsNullOrEmpty(license))
            {
                string path = Path.Combine(workingDirectory, license);
                if (File.Exists(path))
                {
                    package.License = File.ReadAllText(path);
                }
            }

            return package;
        }

        private static IEnumerable<string> GetSupportedVersions(XmlDocument doc)
        {
            XmlNodeList list = doc.GetElementsByTagName("InstallationTarget");

            if (list.Count == 0)
                list = doc.GetElementsByTagName("<VisualStudio");

            List<string> versions = new List<string>();

            foreach (XmlNode node in list)
            {
                string raw = node.Attributes["Version"].Value.Trim('[', '(', ']', ')');
                string[] entries = raw.Split(',');

                foreach (string entry in entries)
                {
                    Version v;
                    if (Version.TryParse(entry, out v) && !versions.Contains(v.ToString()))
                    {
                        versions.Add(v.ToString());
                    }
                }
            }

            return versions;
        }

        private string ParseNode(XmlDocument doc, string name, bool required, string attribute = "")
        {
            XmlNodeList list = doc.GetElementsByTagName(name);

            if (list.Count > 0)
            {
                XmlNode node = list[0];

                if (string.IsNullOrEmpty(attribute))
                    return node.InnerText;

                XmlAttribute attr = node.Attributes[attribute];

                if (attr != null)
                    return attr.Value;
            }

            if (required)
            {
                string message = string.Format("Attribute '{0}' could not be found on the '{1}' element in the .vsixmanifest file.", attribute, name);
                throw new Exception(message);
            }

            return null;
        }

    }
}
