using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using VsGalleryCore.Core;

namespace VsGalleryCore.Attributes
{
    public class ApiKeyAuthenticationFilterAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Request.Headers.TryGetValue("Authorization", out var authorizationToken);

            if (StringValues.IsNullOrEmpty(authorizationToken))
            {
                context.Result = new BadRequestObjectResult("Authorization header is missing");
                return;
            }

            var apiKeyToken = authorizationToken.ToString();

            if (!apiKeyToken.StartsWith("ApiKey"))
            {
                context.Result = new BadRequestObjectResult("Authorization type must be ApiKey :)");
                return;
            }
            
            var regex = new Regex(@"ApiKey(\s+)(?<Token>\w*)", RegexOptions.IgnoreCase|RegexOptions.Singleline);

            var match = Regex.Match(apiKeyToken, @"ApiKey(\s+)(?<Token>\w*)",
                RegexOptions.IgnoreCase | RegexOptions.Singleline);

            var token = match.Groups["Token"].Value;

            if (!token.Equals(VsGalleryConfigurationLoader.Configuration.ApiKey))
            {
                context.Result = new UnauthorizedObjectResult("ApiKey is not valid");
            }
        }
        
        
    }
}